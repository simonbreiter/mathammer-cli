module.exports = {
  // Melee Character Stats
  meleeSkillChange: skill => {
    return {
      type: "MELEE_SKILL_CHANGE",
      payload: {
        skill: skill
      }
    };
  },
  meleeStrengthChange: strength => {
    return {
      type: "MELEE_STRENGTH_CHANGE",
      payload: {
        strength: strength
      }
    };
  },
  meleeAPChange: attackPower => {
    return {
      type: "MELEE_AP_CHANGE",
      payload: {
        attackPower: attackPower
      }
    };
  },
  meleeDamageChange: damage => {
    return {
      type: "MELEE_DAMAGE_CHANGE",
      payload: {
        damage: damage
      }
    };
  },
  meleeAttackChange: attack => {
    return {
      type: "MELEE_ATTACK_CHANGE",
      payload: {
        attack: attack
      }
    };
  },
  // Melee Rerolls
  meleeHitRerollChange: hitReroll => {
    return {
      type: "MELEE_HITREROLL_CHANGE",
      payload: {
        hitReroll: hitReroll
      }
    };
  },
  meleeWoundRerollChange: woundReroll => {
    return {
      type: "MELEE_WOUNDREROLL_CHANGE",
      payload: {
        woundReroll: woundReroll
      }
    };
  },
  // Melee Modifiers
  meleeHitModifierChange: hitModifier => {
    return {
      type: "MELEE_HITMODIFIER_CHANGE",
      payload: {
        hitModifier: hitModifier
      }
    };
  },
  meleeWoundModifierChange: woundModifier => {
    return {
      type: "MELEE_WOUNDMODIFIER_CHANGE",
      payload: {
        woundModifier: woundModifier
      }
    };
  },
  // Ballistic Character Stats
  ballisticSkillChange: skill => {
    return {
      type: "BALLISTIC_SKILL_CHANGE",
      payload: {
        skill: skill
      }
    };
  },
  ballisticStrengthChange: strength => {
    return {
      type: "BALLISTIC_STRENGTH_CHANGE",
      payload: {
        strength: strength
      }
    };
  },
  ballisticAPChange: attackPower => {
    return {
      type: "BALLISTIC_AP_CHANGE",
      payload: {
        attackPower: attackPower
      }
    };
  },
  ballisticDamageChange: damage => {
    return {
      type: "BALLISTIC_DAMAGE_CHANGE",
      payload: {
        damage: damage
      }
    };
  },
  ballisticAttackChange: attack => {
    return {
      type: "BALLISTIC_ATTACK_CHANGE",
      payload: {
        attack: attack
      }
    };
  },
  // Ballistic Rerolls
  ballisticHitRerollChange: hitReroll => {
    return {
      type: "BALLISTIC_HITREROLL_CHANGE",
      payload: {
        hitReroll: hitReroll
      }
    };
  },
  ballisticWoundRerollChange: woundReroll => {
    return {
      type: "BALLISTIC_WOUNDREROLL_CHANGE",
      payload: {
        woundReroll: woundReroll
      }
    };
  },
  // Ballistic Modifiers
  ballisticHitModifierChange: hitModifier => {
    return {
      type: "BALLISTIC_HITMODIFIER_CHANGE",
      payload: {
        hitModifier: hitModifier
      }
    };
  },
  ballisticWoundModifierChange: woundModifier => {
    return {
      type: "BALLISTIC_WOUNDMODIFIER_CHANGE",
      payload: {
        woundModifier: woundModifier
      }
    };
  },
  enemyInvulnerableSaveChange: invulnerableSave => {
    return {
      type: "ENEMY_INVULNERABLESAVE_CHANGE",
      payload: {
        invulnerableSave: invulnerableSave
      }
    };
  },
  calculate: state => {
    return {
      type: "CALCULATE",
      payload: state
    };
  }
};
