const {
  hitProbability,
  woundProbability,
  damageProbability
} = require("mathammer");

const initialState = {};

module.exports = {
  mathammerApp: (state = initialState, action) => {
    switch (action.type) {
      // Melee Character Stats
      case "MELEE_SKILL_CHANGE":
        return {
          ...state,
          model: {
            melee: {
              skill: action.payload.skill
            }
          }
        };
      case "MELEE_STRENGTH_CHANGE":
        return {
          ...state,
          model: {
            melee: {
              ...state.model.melee,
              strength: action.payload.strength
            }
          }
        };
      case "MELEE_AP_CHANGE":
        return {
          ...state,
          model: {
            melee: {
              ...state.model.melee,
              attackPower: action.payload.attackPower
            }
          }
        };
      case "MELEE_DAMAGE_CHANGE":
        return {
          ...state,
          model: {
            melee: {
              ...state.model.melee,
              damage: action.payload.damage
            }
          }
        };
      case "MELEE_ATTACK_CHANGE":
        return {
          ...state,
          model: {
            melee: {
              ...state.model.melee,
              attack: action.payload.attack
            }
          }
        };
      // Melee Reroll
      case "MELEE_HITREROLL_CHANGE":
        return {
          ...state,
          hitReroll: {
            melee: action.payload.hitReroll
          }
        };
      case "MELEE_WOUNDREROLL_CHANGE":
        return {
          ...state,
          woundReroll: {
            melee: action.payload.woundReroll
          }
        };
      // Melee Modifier
      case "MELEE_HITMODIFIER_CHANGE":
        return {
          ...state,
          hitModifier: {
            melee: action.payload.hitModifier
          }
        };
      case "MELEE_WOUNDMODIFIER_CHANGE":
        return {
          ...state,
          woundModifier: {
            melee: action.payload.woundModifier
          }
        };
      // Ballistic Character Stats
      case "BALLISTIC_SKILL_CHANGE":
        return {
          ...state,
          model: {
            ballistic: {
              skill: action.payload.skill
            }
          }
        };
      case "BALLISTIC_STRENGTH_CHANGE":
        return {
          ...state,
          model: {
            ballistic: {
              ...state.model.ballistic,
              strength: action.payload.strength
            }
          }
        };
      case "BALLISTIC_AP_CHANGE":
        return {
          ...state,
          model: {
            ballistic: {
              ...state.model.ballistic,
              attackPower: action.payload.attackPower
            }
          }
        };
      case "BALLISTIC_DAMAGE_CHANGE":
        return {
          ...state,
          model: {
            ballistic: {
              ...state.model.ballistic,
              damage: action.payload.damage
            }
          }
        };
      case "BALLISTIC_ATTACK_CHANGE":
        return {
          ...state,
          model: {
            ballistic: {
              ...state.model.ballistic,
              attack: action.payload.attack
            }
          }
        };
      // Ballistic Reroll
      case "BALLISTIC_HITREROLL_CHANGE":
        return {
          ...state,
          hitReroll: {
            ballistic: action.payload.hitReroll
          }
        };
      case "BALLISTIC_WOUNDREROLL_CHANGE":
        return {
          ...state,
          woundReroll: {
            ballistic: action.payload.woundReroll
          }
        };
      // Ballistic Modifier
      case "BALLISTIC_HITMODIFIER_CHANGE":
        return {
          ...state,
          hitModifier: {
            ballistic: action.payload.hitModifier
          }
        };
      case "BALLISTIC_WOUNDMODIFIER_CHANGE":
        return {
          ...state,
          woundModifier: {
            ballistic: action.payload.woundModifier
          }
        };
      case "ENEMY_INVULNERABLESAVE_CHANGE":
        return {
          ...state,
          enemy: {
            ...state.enemy,
            invulnerableSave: action.payload.invulnerableSave
          }
        };
      case "CALCULATE":
        const hitProb = hitProbability(action.payload);
        const probHit = {
          ...state,
          hitProbability: hitProb
        };
        return {
          ...state,
          hitProbability: hitProb
        };
      default:
        return state;
    }
  }
};
