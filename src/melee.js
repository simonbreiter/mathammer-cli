module.exports = {
  statsMelee: [
    {
      type: "input",
      name: "skill",
      message: "Skill:"
    }
  ],
  weaponMelee: [
    {
      type: "input",
      name: "strength",
      message: "Strength:"
    }
  ],
  apMelee: [
    {
      type: "input",
      name: "attackPower",
      message: "Attack Power:"
    }
  ],
  damageValueMelee: [
    {
      type: "input",
      name: "damage",
      message: "Damage:"
    }
  ],
  attackMelee: [
    {
      type: "input",
      name: "attack",
      message: "Attacks:"
    }
  ],
  modifierHitMelee: [
    {
      type: "input",
      name: "hitModifier",
      message: "Hit Modifier:"
    }
  ],
  modifierWoundMelee: [
    {
      type: "input",
      name: "woundModifier",
      message: "Wound Modifier:"
    }
  ],
  rerollHitMelee: [
    {
      type: "list",
      name: "hitReroll",
      message: "Do you have a Hit Reroll?",
      choices: ["reroll-none", "reroll-1", "reroll-all"]
    }
  ],
  rerollWoundMelee: [
    {
      type: "list",
      name: "woundReroll",
      message: "Do you have a Wound Reroll?",
      choices: ["reroll-none", "reroll-1", "reroll-all"]
    }
  ]
};
