const redux = require('redux')
const actions = require('./actions/actions')
const reducers = require('./reducers/reducers')
const inquirer = require('inquirer')
const questions = require('./questions')
const melee = require('./melee')
const ballistic = require('./ballistic')
const enemy = require('./enemy')
const Table = require('cli-table')
const colors = require('colors')
const {
  hitProbability,
  woundProbability,
  damageProbability
} = require('mathammer')

const store = redux.createStore(reducers.mathammerApp)

function main () {
  console.log('                    __  __')
  console.log(
    '   ____ ___  ____ _/ /_/ /_  ____ _____ ___  ____ ___  ___  _____'
  )
  console.log(
    '  / __ `__ \\/ __ `/ __/ __ \\/ __ `/ __ `__ \\/ __ `__ \\/ _ \\/ ___/'
  )
  console.log(' / / / / / / /_/ / /_/ / / / /_/ / / / / / / / / / / /  __/ /')
  console.log(
    '/_/ /_/ /_/\\__,_/\\__/_/ /_/\\__,_/_/ /_/ /_/_/ /_/ /_/\\___/_/\n'
  )

  inquirer
    .prompt(questions.questions)

    .then(answers => {
      if (answers.type === 'Melee') {
        meleeStat()
      } else {
        ballisticStat()
      }
    })
}

function meleeStat () {
  inquirer
    .prompt(melee.statsMelee)
    .then(answers => {
      store.dispatch(actions.meleeSkillChange(answers.skill))
    })
    .then(weaponsMelee)
}

function ballisticStat () {
  inquirer
    .prompt(ballistic.statsBallistic)
    .then(answers => {
      store.dispatch(actions.ballisticSkillChange(answers.skill))
    })
    .then(weaponsBallistic)
}

function weaponsMelee () {
  inquirer
    .prompt(melee.weaponMelee)
    .then(answers => {
      store.dispatch(actions.meleeStrengthChange(answers.strength))
    })
    .then(APMelee)
}

function APMelee () {
  inquirer
    .prompt(melee.apMelee)
    .then(answers => {
      store.dispatch(actions.meleeAPChange(answers.attackPower))
    })
    .then(damageMelee)
}

function damageMelee () {
  inquirer
    .prompt(melee.damageValueMelee)
    .then(answers => {
      store.dispatch(actions.meleeDamageChange(answers.damage))
    })
    .then(attacksMelee)
}

function attacksMelee () {
  inquirer
    .prompt(melee.attackMelee)
    .then(answers => {
      store.dispatch(actions.meleeAttackChange(answers.attack))
    })
    .then(hitRerollMelee)
}

function hitRerollMelee () {
  inquirer
    .prompt(melee.rerollHitMelee)
    .then(answers => {
      store.dispatch(actions.meleeHitRerollChange(answers.hitReroll))
    })
    .then(woundRerollMelee)
}

function woundRerollMelee () {
  inquirer
    .prompt(melee.rerollWoundMelee)
    .then(answers => {
      store.dispatch(actions.meleeWoundRerollChange(answers.woundReroll))
    })
    .then(modifiersHitMelee)
}

function modifiersHitMelee () {
  inquirer
    .prompt(melee.modifierHitMelee)
    .then(answers => {
      store.dispatch(actions.meleeHitModifierChange(answers.hitModifier))
    })
    .then(modifiersWoundMelee)
}

function modifiersWoundMelee () {
  inquirer
    .prompt(melee.modifierWoundMelee)
    .then(answers => {
      store.dispatch(actions.meleeWoundModifierChange(answers.woundModifier))
    })
    .then(invulnerableSaves)
}

function weaponsBallistic () {
  inquirer
    .prompt(ballistic.weaponBallistic)
    .then(answers => {
      store.dispatch(actions.ballisticStrengthChange(answers.strength))
    })
    .then(APBallistic)
}

function APBallistic () {
  inquirer
    .prompt(ballistic.apBallistic)
    .then(answers => {
      store.dispatch(actions.ballisticAPChange(answers.attackPower))
    })
    .then(damageBallistic)
}

function damageBallistic () {
  inquirer
    .prompt(ballistic.damageValueBallistic)
    .then(answers => {
      store.dispatch(actions.ballisticDamageChange(answers.damage))
    })
    .then(attacksBallistic)
}

function attacksBallistic () {
  inquirer
    .prompt(ballistic.attackBallistic)
    .then(answers => {
      store.dispatch(actions.ballisticAttackChange(answers.attack))
    })
    .then(hitRerollBallistic)
}

function hitRerollBallistic () {
  inquirer
    .prompt(ballistic.rerollHitBallistic)
    .then(answers => {
      store.dispatch(actions.ballisticHitRerollChange(answers.hitReroll))
    })
    .then(woundRerollBallistic)
}

function woundRerollBallistic () {
  inquirer
    .prompt(ballistic.rerollWoundBallistic)
    .then(answers => {
      store.dispatch(actions.ballisticWoundRerollChange(answers.woundReroll))
    })
    .then(modifiersHitBallistic)
}

function modifiersHitBallistic () {
  inquirer
    .prompt(ballistic.modifierHitBallistic)
    .then(answers => {
      store.dispatch(actions.ballisticHitModifierChange(answers.hitModifier))
    })
    .then(modifiersWoundBallistic)
}

function modifiersWoundBallistic () {
  inquirer
    .prompt(ballistic.modifierWoundBallistic)
    .then(answers => {
      store.dispatch(
        actions.ballisticWoundModifierChange(answers.woundModifier)
      )
    })
    .then(invulnerableSaves)
}

function invulnerableSaves () {
  inquirer
    .prompt(enemy.invulnerableSave)
    .then(answers => {
      const table = new Table({
        chars: {
          top: '═',
          'top-mid': '╤',
          'top-left': '╔',
          'top-right': '╗',
          bottom: '═',
          'bottom-mid': '╧',
          'bottom-left': '╚',
          'bottom-right': '╝',
          left: '║',
          'left-mid': '╟',
          mid: '─',
          'mid-mid': '┼',
          right: '║',
          'right-mid': '╢',
          middle: '│'
        },
        style: {
          'padding-left': 2,
          'padding-right': 2
        },
        head: [
          '',
          'T2'.black,
          'T3'.black,
          'T4'.black,
          'T5'.black,
          'T6'.black,
          'T7'.black,
          'T8'.black
        ]
      })

      const roundToTwoDigits = number => Math.round(number * 1000) / 1000

      store.dispatch(
        actions.enemyInvulnerableSaveChange(answers.invulnerableSave)
      )

      store.dispatch(actions.calculate(store.getState()))

      const state = store.getState()

      for (let save = 2; save <= 6; save++) {
        values = []
        for (var toughness = 2; toughness <= 8; toughness++) {
          const probHit = {
            ...state,
            enemy: {
              toughness: toughness
            }
          }

          const probWound = {
            ...state,
            enemy: {
              ...state.enemy,
              save: save
            },
            woundProbability: woundProbability(probHit)
          }

          const dmgProb = damageProbability(probWound)

          if (dmgProb.hasOwnProperty('melee')) {
            if (dmgProb.melee < 0.2) {
              values.push(colors.red(roundToTwoDigits(dmgProb.melee)))
            } else if (dmgProb.melee <= 0.5) {
              values.push(colors.yellow(roundToTwoDigits(dmgProb.melee)))
            } else {
              values.push(colors.green(roundToTwoDigits(dmgProb.melee)))
            }
          } else {
            if (dmgProb.ballistic < 0.2) {
              values.push(colors.red(roundToTwoDigits(dmgProb.ballistic)))
            } else if (dmgProb.ballistic <= 0.5) {
              values.push(colors.yellow(roundToTwoDigits(dmgProb.ballistic)))
            } else {
              values.push(colors.green(roundToTwoDigits(dmgProb.ballistic)))
            }
          }
        }
        table.push({
          [`Sv${save}`.black]: values
        })
      }
      console.log(`\n${table.toString()}`)
    })
    .catch(error => {
      console.log(error)
    })
}

main()
