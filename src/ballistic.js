module.exports = {
  statsBallistic: [
    {
      type: 'input',
      name: 'skill',
      message: 'Skill:',
      validate: answer => {
        if (answer && !isNaN(answer) && answer >= 2 && answer <= 5) {
          return true
        } else {
          return 'Please enter a number between 2 and 5'
        }
      }
    }
  ],
  weaponBallistic: [
    {
      type: 'input',
      name: 'strength',
      message: 'Strength:',
      validate: answer => {
        if (answer && !isNaN(answer)) {
          return true
        } else {
          return 'Please enter a number'
        }
      }
    }
  ],
  apBallistic: [
    {
      type: 'input',
      name: 'attackPower',
      message: 'Attack Power:',
      default: 0
    }
  ],
  damageValueBallistic: [
    {
      type: 'input',
      name: 'damage',
      message: 'Damage:',
      default: 1
    }
  ],
  attackBallistic: [
    {
      type: 'input',
      name: 'attack',
      message: 'Attacks:',
      default: 1
    }
  ],
  modifierHitBallistic: [
    {
      type: 'input',
      name: 'hitModifier',
      message: 'Hit Modifier:',
      default: 0
    }
  ],
  modifierWoundBallistic: [
    {
      type: 'input',
      name: 'woundModifier',
      message: 'Wound Modifier:',
      default: 0
    }
  ],
  rerollHitBallistic: [
    {
      type: 'list',
      name: 'hitReroll',
      message: 'Do you have a Hit Reroll?',
      choices: [
        {
          name: 'no reroll',
          value: 'reroll-none'
        },
        {
          name: 'reroll ones',
          value: 'reroll-1'
        },
        {
          name: 'reroll all',
          value: 'reroll-all'
        }
      ]
    }
  ],
  rerollWoundBallistic: [
    {
      type: 'list',
      name: 'woundReroll',
      message: 'Do you have a Wound Reroll?',
      choices: [
        {
          name: 'no reroll',
          value: 'reroll-none'
        },
        {
          name: 'reroll ones',
          value: 'reroll-1'
        },
        {
          name: 'reroll all',
          value: 'reroll-all'
        }
      ]
    }
  ]
}
