module.exports = {
  invulnerableSave: [
    {
      type: 'input',
      name: 'invulnerableSave',
      message: 'invulnerable Save:',
      default: 0
    }
  ]
}
